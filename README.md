# surfn-icons

Surfn, a colourful icon theme

https://github.com/erikdubois/Surfn

<br><br>
Hot to clone this repository:
```
git clone https://gitlab.com/azul4/content/icons-and-themes/surfn-icons.git
```
<br>

## NOTE: Do not compile in chroot environment


